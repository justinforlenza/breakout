import ddf.minim.*;

Minim minim;
AudioPlayer brk;
AudioPlayer pong;
AudioPlayer chnce;
AudioPlayer wawa;
AudioPlayer over;
PImage bg;

float ballX = 700, ballY = 500, ballSpeed = 1.52, seconds = 0, score = 0, startTime,multiplier=1;
int paddleX = 300, xSpeed = 1, ySpeed = 1, life = 5, currentBuff, isNew, paddleWidth = 115, BallSize = 30, buffTime, buffPrevType, buffType,logic=0;
block[][] blocks = new block[4][9];

void setup() {
  size(1400, 800);
  bg = loadImage("bg.jpg");
  noCursor();
  minim = new Minim(this);
  brk = minim.loadFile("break.wav");
  pong = minim.loadFile("pong.wav");
  chnce = minim.loadFile("chance.wav");
  wawa = minim.loadFile("wawa.wav");
  over = minim.loadFile("gameover.mp3");
  for (int i = 0; i < blocks.length; i +=2 ) {
    for (int k = 0; k < blocks[i].length; k ++) {
      blocks[i][k] = new block((k + 105)+k*150, (i+100)+i*85);
    }
  }
  for (int i = 1; i < blocks.length; i +=2 ) {
    for (int k = 0; k < blocks[i].length; k ++) {
      blocks[i][k] = new block((k + 85)+k*150, (i+100)+i*85);
    }
  }
}

void draw() {
  background(bg);
  //background(125);
  for (int i = 0; i < blocks.length; i ++) {
    for (int k = 0; k < blocks[i].length; k ++) {
      blocks[i][k].display();
    }
  }
  fill(255);
  ball();
  paddle();
  checkCollide();
  checkBreak();
  score();
  lifeCounter(null);
  buff();
}

void checkCollide() {
  if (ballX >= 1393) {
    ballSpeed += .1;
    xSpeed = -xSpeed;
    pong.rewind();
    pong.play();
  } else if (ballX <= BallSize/2) {
    ballSpeed += .1;
    xSpeed = -xSpeed;
    pong.rewind();
    pong.play();
  }
  if (ballY <= BallSize/2) {
    ballSpeed += .1;
    ySpeed = -ySpeed;
    pong.rewind();
    pong.play();
  }
  if (ballY >= (height-35)-(BallSize/2)) {
    if (ballX >= mouseX && ballX <= mouseX+(paddleWidth/2)) {
      ballSpeed += .1;
      xSpeed = 1;
      ySpeed = -1;
      pong.rewind();
      pong.play();
    } 
    if (ballX > mouseX-(paddleWidth/2) && ballX < mouseX) {
      ballSpeed += .1;
      xSpeed = -1;
      ySpeed = -1;
      pong.rewind();
      pong.play();
    }
  }
  if (ballY > 800) {
    noLoop();
    ballX = 700;
    ballY = 500;
    lifeCounter("remove");
  }
}

void ball() {
  ballX = ballX + xSpeed * ballSpeed;
  ballY = ballY + ySpeed * ballSpeed;
  ellipseMode(CENTER);
  ellipse(ballX, ballY, BallSize, BallSize);
}

void paddle() {
  rectMode(CENTER);
  rect(mouseX, 775, paddleWidth, 25);
}

void keyPressed() {
  if (key == ' ' && life > 0) {
    loop();
  }
}

void reset() {
  ballX = 700;
  ballY = 500;
  ballSpeed = 1;
}
void checkBreak() {
  if (millis() - seconds >= 75) {
    for (int i = 0; i < blocks.length; i ++) {
      for (int k = 0; k < blocks[i].length; k ++) {
        int blockY = blocks[i][k].ypos;
        int blockX = blocks[i][k].xpos;
        if (ballX <= blockX + (72 + (BallSize/2)) && ballX >= blockX - (72 + (BallSize/2)) && blocks[i][k].display != 1) {
          if (ballY <= blockY + (40 + (BallSize/2)) && ballY >= blockY + 40) {
            blocks[i][k].collide();
            ySpeed = -ySpeed;
            ballSpeed += .025;
            seconds = millis();
            score += multiplier * 15;
            if (blocks[i][k].type == 1) {
              buffChange();
            } else {
              brk.rewind();
              brk.play();
            }
            break;
          } else if (ballY <= blockY - 40 && ballY >= blockY - (40 + (BallSize/2)) && blocks[i][k].display != 1) {
            blocks[i][k].collide();
            ySpeed = -ySpeed;
            ballSpeed += .025;
            seconds = millis();
            if (blocks[i][k].type == 1) {
              buffChange();
            } else {
              brk.rewind();
              brk.play();
            }
            break;
          }
        }
        if (ballY <= blockY + 40 && ballY >= blockY - 40 && blocks[i][k].display != 1) {
          if (ballX <= blockX + (72 + (BallSize/2)) && ballX >= blockX + 72) {
            blocks[i][k].collide();
            xSpeed = -xSpeed;
            ballSpeed += .025;
            seconds = millis();
            if (blocks[i][k].type == 1) {
              buffChange();
            } else {
              brk.rewind();
              brk.play();
            }
            break;
          } else if (ballX >= blockX - (72 + (BallSize/2)) && ballX <= blockX - 72 && blocks[i][k].display != 1) {
            blocks[i][k].collide();
            xSpeed = -xSpeed;
            ballSpeed += .025;
            seconds = millis();
            if (blocks[i][k].type == 1) {
              buffChange();
            } else {
              brk.rewind();
              brk.play();
            }
            break;
          }
        }
      }
    }
  }
}
void printMouse() {
  println("X : " + mouseX + " , Y: " + mouseY);
}
void score() {
  textSize(24);
  textAlign(CENTER, CENTER);
  fill(255);
  stroke(0);
  text("Score : " + score, 500, 25);
}
void lifeCounter(String type) {
  if (type == "remove") {
    life -= 1;
    if (life == 0) {
      noLoop();
      textSize(64);
      textAlign(CENTER, CENTER);
      fill(0);
      text("Game Over", 700, 400);
      currentBuff = 0;
      multiplier = 1;
      BallSize = 30;
      over.rewind();
      over.play();
    } else {
      wawa.rewind();
      wawa.play();
    }
  } else {
    textSize(24);
    textAlign(CENTER, CENTER);
    fill(255);
    stroke(0);
    text("Lives : " + life, 900, 25);
  }
}

void buffChange() {
  if (currentBuff == 2 && buffType == 1) {
    ballSpeed = ballSpeed/2;
  } else if (currentBuff == 2 && buffType == 0) {
    ballSpeed = ballSpeed*2;
  }
  multiplier = 1;
  BallSize = 30;
  buffTime = int(random(10, 20));
  int rnd = int(random(0, 100));
  buffPrevType = buffType;
  if (buffPrevType == 1){
   logic = -25; 
  } else if (buffPrevType == 0) {
   logic = 25; 
  }
  int rnd2 = int(random(0, 100));
  if (rnd2 >= 0 && rnd >= 50+logic) {
    buffType = 1;
  } else {
    buffType = 0;
  }
  if (rnd >= 0 && rnd < 24) {
    currentBuff = 1;
    isNew = 1;
    textAlign(CENTER, CENTER);
    fill(255);
    if (buffType == 1) {
      text("Get DOUBLE Points for the next " + buffTime + "seconds!", 500, 200);
      chnce.rewind();
      chnce.play();
    } else {
      text("Get HALF Points for the next " + buffTime + "seconds!", 500, 200);
      wawa.rewind();
      wawa.play();
    }
    text("Press Space to continue", 500, 250);
    noLoop();
  } else if (rnd >= 24 && rnd < 48 && currentBuff != 2) {
    currentBuff = 2;
    isNew = 1;
    textAlign(CENTER, CENTER);
    fill(255);
    if (buffType == 1) {
      text("Ball speed is increased for the next "+ buffTime + " seconds!", 500, 200);
      chnce.rewind();
      chnce.play();
    } else {
      text("Ball speed is decreased for the next "+ buffTime + " seconds!", 500, 200);
      wawa.rewind();
      wawa.play();
    }
    text("Press Space to continue", 500, 250);
    noLoop();
  } else if (rnd >= 48 && rnd < 72) {
    currentBuff = 3;
    textAlign(CENTER, CENTER);
    fill(255);
    if (buffType == 1) {
      text("Paddle Size has been increased!", 500, 200);
      chnce.rewind();
      chnce.play();
    } else {
      text("Paddle Size has been decreased!", 500, 200);
      wawa.rewind();
      wawa.play();
    }
    text("Press Space to continue", 500, 250);
    noLoop();
  } else if (rnd >= 72 && rnd < 96) {
    currentBuff = 4;
    isNew = 1;
    textAlign(CENTER, CENTER);
    fill(255);
    if (buffType == 1) {
      text("Ball size is increased for the next "+ buffTime + " seconds!", 500, 200);
      chnce.rewind();
      chnce.play();
    } else {
      text("Ball size is decreased for the next "+ buffTime + " seconds!", 500, 200);
      wawa.rewind();
      wawa.play();
    }
    text("Press Space to continue", 500, 250);
    noLoop();
  } else if (rnd >= 96 && rnd < 100) {
    currentBuff = 5;
    textAlign(CENTER, CENTER);
    fill(255);
    text("Recieve 1 Life!", 500, 200);
    text("Press Space to continue", 500, 250);
    chnce.rewind();
    chnce.play();
    noLoop();
  }
}
void buff() {
  if (currentBuff == 1 && isNew == 1) {
    startTime = millis();
    isNew = 0;
    if (buffType == 1) {
      multiplier = 2;
    } else {
      multiplier = .5;
    }
  } else if (currentBuff == 1 && isNew == 0) {
    textAlign(CENTER, CENTER);
    fill(255);
    text("Buff:" + (millis() - startTime)/1000, 100, 25);
    if ( startTime + (buffTime * 1000) < millis()) {
      currentBuff = 0;
      multiplier = 1;
    }
  } else if (currentBuff == 2 && isNew == 1) {
    startTime = millis();
    isNew = 0;
    if (buffType == 1) {
      ballSpeed = ballSpeed*2;
    } else {
      ballSpeed = ballSpeed/2;
    }
  } else if (currentBuff == 2 && isNew == 0) {
    textAlign(CENTER, CENTER);
    fill(255);
    text("Buff:" + (millis() - startTime)/1000, 100, 25);
    if ( startTime + (buffTime * 1000) < millis()) {
      currentBuff = 0;
      isNew = 2;
      if (buffType == 1) {
        ballSpeed = ballSpeed/2;
      } else {
        ballSpeed = ballSpeed*2;
      }
    }
  } else if (currentBuff == 3) {
    if (buffType == 1) {
      paddleWidth+=15;
    } else {
      paddleWidth-=15;
    }
    currentBuff = 0;
    isNew = 2;
  } else if (currentBuff == 4 && isNew == 1) {
    startTime = millis();
    isNew = 0;
    if (buffType == 1) {
      BallSize += 15;
    } else {
      BallSize -= 15;
    }
  } else if (currentBuff == 4 && isNew == 0) {
    textAlign(CENTER, CENTER);
    fill(255);
    text("Buff:" + (millis() - startTime)/1000, 100, 25);  
    if ( startTime + (buffTime * 1000) < millis()) {
      currentBuff = 0;
      isNew = 2;
      BallSize = 30;
    }
  } else if (currentBuff == 5) {
    life = life + 1;
    currentBuff = 0;
    isNew = 2;
  }
}

